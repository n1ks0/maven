ARG BASE_IMAGE_TAG=3.8-eclipse-temurin-8-alpine
FROM maven:${BASE_IMAGE_TAG}

RUN apk add --no-cache git
